package tests;

import org.openqa.selenium.WebDriver;
import pages.LoginPage;
import utils.BaseTest;

//This program checks login to PrestaShop's admin panel

public class Main extends BaseTest {

    public static void main(String[] args) {
        WebDriver driver = BaseTest.getConfiguredDriver();

        LoginPage loginPage = new LoginPage(driver);

        loginPage.open();
        loginPage.fillEmailInput();
        loginPage.fillPasswordInput();
        loginPage.clickLoginBtn();

        quiteDriver(driver);
    }
}
